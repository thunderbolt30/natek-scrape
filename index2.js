const puppeteer = require('puppeteer');
const fs = require('fs');


(async () => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto('https://demo-shop.natek.eu', {
        waitUntil: 'networkidle2'
    });
    let arr = []
    let productUrl=[];
    
    let hasNextPage = true
    let categoriesHierrachy = await page.evaluate(()=>{
        let temporaryCategories = Array.from(document.querySelectorAll('ul.product-categories> li')).map(category => category.innerText.trim());
        return temporaryCategories
    })
    while(hasNextPage){ 
        let pageUrls = await page.evaluate(()=>{
            let urls = Array.from(document.querySelectorAll('ul.columns-4 li a.woocommerce-loop-product__link ')).map(prodUrl => prodUrl.href)
            return urls
        })
        productUrl = [...productUrl, ...pageUrls]
        if(await page.$('li a.next')){
            await page.click('li a.next')
        }else{
            hasNextPage=false
        }
}            
    
    for (let i = 0; i < productUrl.length; i++){
        let productColorOptions = 'N/A';
        let productSizeOptions= 'N/A';
        let relatedSKUS = 'N/A'
        let logo ='N/A';
        let productLogoOptions = 'N/A';
        let productSize = 'N/A'
        let productColor = 'N/A'
        
        
        await page.goto(`${productUrl[i]}`, {
            waitUntil: 'networkidle2'
        });
        if (await page.$('ul.columns-4 li a.woocommerce-loop-product__link')) {
            relatedSKUS = await page.evaluate(()=>{
                let skus = Array.from(document.querySelectorAll('ul.columns-4 li a.ajax_add_to_cart')).map(product => product.dataset.product_sku);
                return skus
            })    
        }
        if (await page.$('ul.columns-4 li a.add_to_cart_button')) {
            relatedSKUS = await page.evaluate(()=>{
                let skus = Array.from(document.querySelectorAll('ul.columns-4 li a.add_to_cart_button')).map(product => product.dataset.product_sku);
                return skus
            })    
        }
    
        let productImage = await page.evaluate(()=>{
            let img = Array.from(document.querySelectorAll('img.zoomImg')).map(product => product.currentSrc);
            return img[0]
        })
        let productName = await page.evaluate(() => {
            let names = document.querySelector('h1.entry-title');
            return names.innerHTML
        })
        const colorOptions = await page.evaluate(() => {
            let names = Array.from(document.querySelectorAll('select#pa_color option.attached')).map(category => category.value);
            return names
        })
        if (await page.$('select#pa_color')) {    
            for (let j = 0; j < colorOptions.length; j++) {
                await page.select("select#pa_color", `${colorOptions[j]}`);
                if (await page.$('select#logo')) {
                    const logoOptions = await page.evaluate(()=>{
                        let logos = Array.from(document.querySelectorAll('select#logo option.attached')).map(category => category.value);
                        return logos
                    })
                    for (let l = 0; l < logoOptions.length; l++) {
                        await page.select("select#logo", `${logoOptions[l]}`);
                        let productPrice;
                        if(await page.$('div.entry-summary ins bdi')){
                            productPrice = await page.evaluate(() => {
                                let price = document.querySelector('div.entry-summary ins bdi');
                                return price.textContent 
                        })}    
                        else if(await page.$('span.price bdi')) {
                            productPrice = await page.evaluate(() => {
                            let price = document.querySelector('span.price bdi');
                            return price.textContent
                        })
                        }else{
                            productPrice = await page.evaluate(() => {
                            let price = document.querySelector('bdi');
                            return price.textContent
                        })}
                        if( await page.$('li#tab-title-additional_information a')){
                            await page.click('li#tab-title-additional_information a')
                            productColorOptions = await page.evaluate(()=>{
                                let color = document.querySelector('td.woocommerce-product-attributes-item__value');
                                return color.innerText
                            });
                            if (await page.$('tr.woocommerce-product-attributes-item--attribute_pa_size td')) {
                                productSizeOptions = await page.evaluate(()=>{
                                    let size = Array.from (document.querySelectorAll('tr.woocommerce-product-attributes-item--attribute_pa_size td'));
                                    if (size.length == 1 ) {
                                        return size[0].outerText
                                    }
                                })
                            }
                            if (await page.$('tr.woocommerce-product-attributes-item--attribute_logo td')) {
                                productLogoOptions = await page.evaluate(()=>{
                                    let logo = Array.from (document.querySelectorAll('tr.woocommerce-product-attributes-item--attribute_logo td'));
                                    if (logo.length == 1 ) {
                                        return logo[0].outerText
                                    }
                                })
                            }
                            
                        }
                        let sku = await page.evaluate(()=>{
                            let skus = document.querySelector('span.sku');
                            return skus.innerText
                        });
                        let productDescrpition = await page.evaluate(()=>{
                            let description = document.querySelector('div.wc-tab p');
                            return description.innerText
                        }); 
                        let productCategory = await page.evaluate(()=>{
                            let category = document.querySelector('span.posted_in a');
                            return category.innerText;
                        });
                        
                        productColor = colorOptions[j];
                        let productLogo = logoOptions[l];
                        let productObj = new Object({});
                        
                        productObj.title = productName;
                        productObj.price = productPrice;
                        productObj.img= productImage;
                        productObj.color = productColor;
                        productObj.logo = productLogo;
                        productObj.sizeOptions = productSizeOptions;
                        productObj.colorOptions= productColorOptions;
                        productObj.logoOptions = productLogoOptions;
                        productObj.sku = sku;
                        productObj.description = productDescrpition;
                        productObj.category = productCategory;
                        productObj.relatedSKUS= relatedSKUS

                        arr.push(productObj)
                    }
                }else if (await page.$('select#pa_size')){
                    const sizeOptions = await page.evaluate(()=>{
                        let sizes = Array.from(document.querySelectorAll('select#pa_size option.attached')).map(size => size.value);
                        return sizes
                    })
                    for (let s = 0; s < sizeOptions.length; s++) {
                        await page.select("select#pa_size", `${sizeOptions[s]}`);
                        let productObj = new Object({})
                    productSize = sizeOptions[s]
                    productColor = colorOptions[j]
                    productObj.title = productName
                    productObj.price = await page.evaluate(() => {
                    let price = document.querySelector('div.woocommerce-variation-price span.price bdi');
                    return price.textContent})
                    if( await page.$('li#tab-title-additional_information a')){
                        await page.click('li#tab-title-additional_information a')
                        productColorOptions = await page.evaluate(()=>{
                            let color = document.querySelector('td.woocommerce-product-attributes-item__value');
                            return color.innerText
                        });
                        
                    }
                    if (await page.$('tr.woocommerce-product-attributes-item--attribute_pa_size td')) {
                        productSizeOptions = await page.evaluate(()=>{
                            let size = Array.from (document.querySelectorAll('tr.woocommerce-product-attributes-item--attribute_pa_size td'));
                            if (size.length == 1 ) {
                                return size[0].outerText
                            }
                        })
                    }
                    let sku = await page.evaluate(()=>{
                        let skus = document.querySelector('span.sku');
                        return skus.innerText
                    });
                    let productDescrpition = await page.evaluate(()=>{
                        let description = document.querySelector('div.wc-tab p');
                        return description.innerText
                    }); 
                    let productCategory = await page.evaluate(()=>{
                        let category = document.querySelector('span.posted_in a');
                        return category.innerText;
                    });
                    productObj.img= productImage;
                    productObj.color = productColor;
                    productObj.size = productSize;
                    
                    productObj.colorOptions = productColorOptions;
                    productObj.sizeOptions = productSizeOptions;
                    productObj.logoOptions = productLogoOptions;
                    productObj.sku = sku;
                    productObj.description = productDescrpition;
                    productObj.category = productCategory;
                    productObj.relatedSKUS = relatedSKUS;
                    arr.push(productObj)
                    }
                    
                }else{
                    if( await page.$('li#tab-title-additional_information a')){
                        await page.click('li#tab-title-additional_information a')
                        productColorOptions = await page.evaluate(()=>{
                            let color = document.querySelector('td.woocommerce-product-attributes-item__value');
                            return color.innerText
                        });
                        if (await page.$('tr.woocommerce-product-attributes-item--attribute_pa_size td')) {
                            productSizeOptions = await page.evaluate(()=>{
                                let size = Array.from (document.querySelectorAll('tr.woocommerce-product-attributes-item--attribute_pa_size td'));
                                if (size.length == 1 ) {
                                    return size[0].outerText
                                }
                            })
                        }
                        
                    }
                let productColor = colorOptions[j]
                let productObj = new Object({})
                productObj.title = productName;
                productObj.img= productImage;
                productObj.sku = sku;
                productObj.category = productCategory;
                productObj.description = productDescrpition;
                productObj.color = productColor;
                productObj.colorOptions= productColorOptions;
                productObj.sizeOptions = productSizeOptions;
                productObj.price = productPrice;
                productObj.relatedSKUS = relatedSKUS;
                productObj.logo = logo;
                arr.push(productObj)
                }
            }

        }else{
            let productPrice;
            if (await page.$('div.entry-summary ins')) {
                productPrice = await page.evaluate(() => {
                    let price = document.querySelector('div.entry-summary ins bdi');
                    return price.textContent
                })
            }else{
            productPrice = await page.evaluate(() => {
            let price = document.querySelector('bdi');
            return price.textContent
        })}
        let sku = await page.evaluate(()=>{
            let skus = document.querySelector('span.sku');
            return skus.innerText
        });
        let productDescrpition = await page.evaluate(()=>{
            let description = document.querySelector('div.wc-tab p');
            return description.innerText
        }); 
        let productCategory = await page.evaluate(()=>{
            let category = document.querySelector('span.posted_in a');
            return category.innerText
        });
        if( await page.$('li#tab-title-additional_information a')){
            await page.click('li#tab-title-additional_information a')
            productColorOptions = await page.evaluate(()=>{
                let color = document.querySelector('td.woocommerce-product-attributes-item__value');
                return color.innerText
            });
            if (await page.$('tr.woocommerce-product-attributes-item--attribute_pa_size td')) {
                productSizeOptions = await page.evaluate(()=>{
                    let size = Array.from (document.querySelectorAll('tr.woocommerce-product-attributes-item--attribute_pa_size td'));
                    if (size.length == 1 ) {
                        return size[0].outerText
                    }
                })
            }
        }
        let productObj = new Object({})
        productObj.title = productName;
        productObj.img= productImage;
        productObj.sku = sku;
        productObj.category = productCategory;
        productObj.description = productDescrpition;
        productObj.colorOptions= productColorOptions;
        productObj.sizeOptions = productSizeOptions;
        productObj.logoOptions = productLogoOptions
        productObj.price = productPrice;
        productObj.relatedSKUS = relatedSKUS;
        productObj.logo = logo;
        productObj.size= productSize;
        productObj.color = productColor;
        arr.push(productObj)
        }
    }
    
    const csvString = [
        ['title','images','sku' ,'category','description' ,'color' , 'size','logo','price','colorOptions','sizeOptions','logoOptions','relatedProductSkus'],
        ...arr.map(item => [item.title,item.img,item.sku ,item.category,item.description,item.color,item.size,item.logo,item.price,item.colorOptions,item.sizeOptions,item.logoOptions,item.relatedSKUS])
        ]
        .map(e => e.join(','))
        let finalResult = csvString.concat(categoriesHierrachy)

    fs.writeFileSync('./outpt.csv',finalResult,'utf-8')

    await browser.close();
})();
